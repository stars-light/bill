package com.example.bill.controller;

import com.example.bill.pojo.Bill;
import com.example.bill.pojo.BillType;
import com.example.bill.service.BillService;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.*;


import java.util.Date;
import java.util.List;

@Controller
@RequestMapping("bill")
public class BillController {

    @Autowired
    private BillService billService;

    //将Type列表数据传给add.html
    @GetMapping("getType")
    public String getType(Model model){

        List<BillType> allTypes = billService.getAllType();
        model.addAttribute("allTypes",allTypes);

        return "/bill/add";
    }

    //接收前端参数,并添加数据到数据库，添加成功后回到列表展示页
    @PostMapping("add")
    //数据可以直接通过表单传递
    public String addBill(int typeId,
                          String title,
                          //日期格式化
                          @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date billTime,
                          Double price,String explain){

        Bill bill = new Bill();
        bill.setTypeId(typeId);
        bill.setTitle(title);
        bill.setBillTime(billTime);
        bill.setPrice(price);
        bill.setExplain(explain);

        billService.addBill(bill);

        return "redirect:/bill/list";
    }

    //多条件查询
    @RequestMapping("list")
    public String selectByParam(
                                @RequestParam(defaultValue = "1") int pageNum,
                                @RequestParam(defaultValue = "10") int pageSize,
                                //此处需给typeId默认值，否则int默认不能为空，将会报错
                                @RequestParam(defaultValue = "0") int typeId,
                                String title,
                                //日期格式化
                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date beginTime,
                                @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date endTime,
                                Model model){


        Bill bill = new Bill();

        if (typeId != 0){
            bill.setTypeId(typeId);
        }

        if (title !=null){
            bill.setTitle(title.trim());
        }

        if (beginTime !=null){
            bill.setBeginTime(beginTime);
        }

        if (endTime !=null){
            bill.setEndTime(endTime);
        }

        //不为空即为搜索条件，存搜索条件
        model.addAttribute("condition",bill);

        PageInfo<Bill> billPageInfo = billService.selectByParam(pageNum, pageSize, bill);

        List<BillType> allTypes = billService.getAllType();

        //给页面存值
        model.addAttribute("pageInfo",billPageInfo);
        model.addAttribute("allTypes",allTypes);


        return "/bill/list";
    }

    //显示信息到修改页面
    @GetMapping("getInfo/{id}")
    public String getInfo(@PathVariable("id") int id,
                          @RequestParam(defaultValue = "1") int pageNum,
                          @RequestParam(defaultValue = "10") int pageSize,
                          Model model){

        //先通过bill_id获取信息
        Bill bill = new Bill();
        bill.setId(id);
        //将信息回显到前端
        PageInfo<Bill> billPageInfo = billService.selectByParam(pageNum, pageSize, bill);

        //只有一个元素，将该元素存到前端
        Bill bill1 = billPageInfo.getList().get(0);

        model.addAttribute("bill",bill1);

        //1.类型全列表信息
        List<BillType> allTypes = billService.getAllType();
        model.addAttribute("allTypes",allTypes);


        return "/bill/update";
    }

    //提交修改信息
    @PostMapping("update")
    public String updateById(
                            int id,
                            int typeId,
                            String title,
                            //日期格式化
                            @DateTimeFormat(iso = DateTimeFormat.ISO.DATE) Date billTime,
                            Double price,
                            String explain){

        Bill bill = new Bill();
        bill.setId(id);
        bill.setTypeId(typeId);
        bill.setTitle(title);
        bill.setBillTime(billTime);
        bill.setPrice(price);
        bill.setExplain(explain);

        //更新
        int i = billService.updateInfoById(bill);

        if (i >= 1 ){
            //post提交数据进行重定向
            return "redirect:/bill/list";
        }

        return "";

    }

    //删除信息
    @GetMapping("delete/{id}")
    public String deleteById(@PathVariable("id") int id){


        int i = billService.deleteInfoById(id);
        if (i >= 1){
            return "redirect:/bill/list";
        }else {
            return "";
        }

    }

}
