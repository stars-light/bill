package com.example.bill.pojo;

import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.annotation.TableField;
import com.baomidou.mybatisplus.annotation.TableId;
import com.baomidou.mybatisplus.annotation.TableName;
import com.baomidou.mybatisplus.core.conditions.Wrapper;
import com.baomidou.mybatisplus.core.conditions.segments.MergeSegments;
import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;


import java.util.Date;

@Data
@AllArgsConstructor
@NoArgsConstructor
@TableName("bill")
public class Bill {

    //id自增
    @TableId(type = IdType.AUTO)
    private int id;

    private String title;

    //表名和属性名对应进行关联
    @TableField("bill_time")
    private Date billTime;

    @TableField("type_id")
    private int typeId;

    private Double price;

    @TableField("`explain`")
    private String explain;

    //false不映射表中的字段
    @TableField(exist = false)
    private String typeName;

    @TableField(exist = false)
    private Date beginTime;

    @TableField(exist = false)
    private Date endTime;


}
