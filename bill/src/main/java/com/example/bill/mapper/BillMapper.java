package com.example.bill.mapper;


import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.bill.pojo.Bill;
import org.apache.ibatis.annotations.Mapper;


import java.util.List;

//需要的是接口

@Mapper
public interface BillMapper extends BaseMapper<Bill> {

    //多条件查询
    List<Bill> selectByParam(Bill bill);

}
