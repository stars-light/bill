package com.example.bill.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import com.example.bill.pojo.BillType;
import org.apache.ibatis.annotations.Mapper;

@Mapper
public interface BillTypeMapper extends BaseMapper<BillType> {
}
