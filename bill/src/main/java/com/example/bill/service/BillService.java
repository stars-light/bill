package com.example.bill.service;


import com.example.bill.mapper.BillMapper;
import com.example.bill.mapper.BillTypeMapper;
import com.example.bill.pojo.Bill;
import com.example.bill.pojo.BillType;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service

public class BillService {
    @Autowired
    private BillMapper billMapper;

    @Autowired
    private BillTypeMapper billTypeMapper;

    public List<Bill> getAllBill(){

        //查询所有数据
        return billMapper.selectList(null);

    }

    public List<BillType> getAllType(){

        //查询所有类型
        return billTypeMapper.selectList(null);
    }

    //通过id查询Type
    public BillType getTypeById(int id){
        return billTypeMapper.selectById(id);
    }

    //添加账单数据
    public int addBill(Bill bill){

        return billMapper.insert(bill);
    }

    //多条件查询
    public PageInfo<Bill> selectByParam(int pageNum, int pageSize, Bill bill){

        return PageHelper.startPage(pageNum,pageSize).doSelectPageInfo(() -> {
            billMapper.selectByParam(bill);
        });
    }

    //更新
    public int updateInfoById(Bill bill){

        return billMapper.updateById(bill);
    }

    //删除信息
    public int deleteInfoById(int id){

        return billMapper.deleteById(id);
    }




}
