package com.example.bill;

import com.example.bill.mapper.BillMapper;
import com.example.bill.pojo.Bill;
import com.example.bill.service.BillService;
import com.github.pagehelper.PageInfo;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.Date;
import java.util.List;

@RunWith(SpringRunner.class)
@SpringBootTest
class BillApplicationTests {


    @Autowired
    private BillMapper billMapper;

    @Autowired
    private BillService billService;


    @Test
    void contextLoads() {

        List<Bill> bills = billMapper.selectList(null);

        bills.forEach(System.out::println);

    }

    @Test
    void contextLoads1() {

        for(int i = 0;i<110;i++){
            String title = "买皮肤" + i;
            int typeId = 3;
            double price = 66.66 + i;
            String explain = "a" + i;
            Date billTime = new Date();

            Bill bill = new Bill();
            bill.setExplain(explain);
            bill.setBillTime(billTime);
            bill.setPrice(price);
            bill.setTypeId(typeId);
            bill.setTitle(title);

            billMapper.insert(bill);

        }

    }

    @Test
    void contextLoads2() {
        Bill bill = new Bill();

        PageInfo<Bill> billPageInfo = billService.selectByParam(1, 10, bill);


        System.out.println(billPageInfo);

        /*
        *
        * PageInfo
        *
        * {
        * pageNum=1,
        * pageSize=10,
        * size=10,
        * startRow=1,
        * endRow=10,
        * total=117,
        * pages=12,
        *
        * list=
        * Page{
        * count=true,
        * pageNum=1,
        * pageSize=10,
        * startRow=0,
        * endRow=10,
        * total=117,
        * pages=12,
        * reasonable=false,
        * pageSizeZero=false}
        *
        * [Bill(id=1, title=交通费, billTime=Mon Jun 26 00:00:00 CST 2017, typeId=2, price=1.23, explain=a, typeName=支出, beginTime=null, endTime=null),
        * Bill(id=2, title=饭补, billTime=Tue Jun 27 00:00:00 CST 2017, typeId=1, price=2.23, explain=b, typeName=收入, beginTime=null, endTime=null),
        * Bill(id=3, title=出差费, billTime=Wed Jun 28 00:00:00 CST 2017, typeId=3, price=66.66, explain=ggg, typeName=借入, beginTime=null, endTime=null),
        * Bill(id=4, title=奖金, billTime=Thu Jun 29 00:00:00 CST 2017, typeId=2, price=44.0, explain=d, typeName=支出, beginTime=null, endTime=null),
        * Bill(id=5, title=演唱会, billTime=Thu Oct 18 00:00:00 CST 2018, typeId=2, price=1.0, explain=hh, typeName=支出, beginTime=null, endTime=null),
        * Bill(id=6, title=采购办公用品, billTime=Wed May 29 00:00:00 CST 2019, typeId=4, price=666.0, explain=lll, typeName=借出, beginTime=null, endTime=null),
        * Bill(id=511, title=游泳, billTime=Tue Jul 13 00:00:00 CST 2021, typeId=3, price=1425.2, explain=fdsf111666, typeName=借入, beginTime=null, endTime=null),
        * Bill(id=514, title=买皮肤0, billTime=Wed Jul 14 00:00:00 CST 2021, typeId=3, price=66.66, explain=a0, typeName=借入, beginTime=null, endTime=null),
        * Bill(id=515, title=买皮肤1, billTime=Wed Jul 14 00:00:00 CST 2021, typeId=3, price=67.66, explain=a1, typeName=借入, beginTime=null, endTime=null),
        * Bill(id=516, title=买皮肤2, billTime=Wed Jul 14 00:00:00 CST 2021, typeId=3, price=68.66, explain=a2, typeName=借入, beginTime=null, endTime=null)],
        *
        * prePage=0,
        * nextPage=2,
        * isFirstPage=true,
        * isLastPage=false,
        * hasPreviousPage=false,
        * hasNextPage=true,
        * navigatePages=8,
        * navigateFirstPage=1,
        * navigateLastPage=8,
        *
        * navigatepageNums=[1, 2, 3, 4, 5, 6, 7, 8]
        * }
         */

    }

}
