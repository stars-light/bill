package com.example.bill.mapper;

import com.example.bill.pojo.BillType;
import org.junit.jupiter.api.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import java.util.List;


@RunWith(SpringRunner.class)
@SpringBootTest
class BillTypeMapperTest {

    @Autowired
    private BillTypeMapper billTypeMapper;

    @Test
    void d(){

        List<BillType> billTypes = billTypeMapper.selectList(null);

        billTypes.forEach(System.out::println);


    }

}